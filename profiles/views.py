from django.shortcuts import render, redirect
from datetime import datetime, date
from .forms import JadwalPribadiForm, DivErrorList
from .models import JadwalPribadi
from django.http import HttpResponseRedirect
from datetime import datetime, date
import calendar

myname = "Nathanael"
titlename = myname.upper
desc = "Hello World! I’m Nathanael, Web designer and programmer with 2 weeks of experience. Holding the record of Worldwide Handsome, thus having spirit to keep learning and never quit. I’ll try hard to create histories with my presence!"
curr_year = int(datetime.now().strftime("%Y"))
birth_date = date(1999, 9, 22)
address = "Griya 15, Depok, West Java"
email = "Evo220999@gmail.com"
phone = "+62-895-0338-6642"
nationality = "Indonesia"


def index(request):
    response = {'myname': myname, 'age': calculate_age(birth_date.year),
                'desc': desc, 'address': address, 'email': email,
                'phone': phone, 'nationality': nationality, 'titlename': titlename}
    return render(request, 'index.html', response)


def guess(request):
    return render(request, 'guess-book.html')


def casun(request):
    return render(request, 'coming-soon.html')


def handler404(request, exception, template_name='404.html'):
    return render(request, '404.html')


def calculate_age(birth_year):
    return curr_year - birth_year if birth_year <= curr_year else 0


def forming(request):
    html = 'forms.html'
    fullset = JadwalPribadi.objects.all()
    if (request.method == "POST"):
        if 'DeleteALL' in request.POST:
            JadwalPribadi.objects.all().delete()
            context = {
                    'form': JadwalPribadiForm(),
                    'fullset': fullset
            }
            return render(request, html, context)
        else:
            form = JadwalPribadiForm(request.POST, error_class=DivErrorList)
            if form.is_valid():
                response = {}
                message = form.save(commit=False)
                response['tanggal'] = request.POST['tanggal']
                response['day'] = getDay(response['tanggal'])
                response['jam'] = request.POST['jam']
                response['nama_kegiatan'] = request.POST['nama_kegiatan']
                response['tempat'] = request.POST['tempat']
                response['kategori'] = request.POST['kategori']
                message = JadwalPribadi(tanggal=response['tanggal'], nama_kegiatan=response['nama_kegiatan'],
                                        tempat=response['tempat'], kategori=response['kategori'],
                                        jam=response['jam'], day=response['day'])
                message.save()
                context = {
                    'form': JadwalPribadiForm(),
                    'fullset': fullset
                }
                return render(request, html, context)
    context = {
            'form': JadwalPribadiForm(),
            'fullset': fullset
        }
    return render(request, 'forms.html', context)


def getDay(dateRaw):
    datetime_object = datetime.strptime(dateRaw, '%Y-%m-%d')
    return calendar.day_name[datetime_object.date().weekday()]
