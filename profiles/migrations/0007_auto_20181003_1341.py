# Generated by Django 2.1.1 on 2018-10-03 06:41

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('profiles', '0006_auto_20181003_1134'),
    ]

    operations = [
        migrations.AlterField(
            model_name='jadwalpribadi',
            name='tanggal',
            field=models.DateField(max_length=200),
        ),
    ]
